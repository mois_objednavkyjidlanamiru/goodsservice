package cz.uhk.goodsservice.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "good")
public class Good {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name", nullable = false)
    @Size(max = 50, message = "Název zboží smí mít maximálně 50 znaků.")
    @NotBlank(message = "Prosím vyplňte název zboží.")
    private String name;

    @Column(name = "quantity")
    @Max(value = 999999999, message = "Počet nesmí přesahovat 9 číslic")
    @Min(value = 0, message = "Počet zboží nesmí být záporná.")
    private int quantity;

    @Column(name = "price")
    @Min(value = 1, message = "Cena nesmí být menší než 1.")
    @Max(value = 999999999, message = "Cena nesmí přesahovat hodnotu 999999999.")
    private double price;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private Status status;

    public Status getStatus() {
        return status;
    }

    public Good() {
    }

    public Good(final String name, final int quantity, final double price, final Status status) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }
}
