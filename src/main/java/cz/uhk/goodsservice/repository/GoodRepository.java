package cz.uhk.goodsservice.repository;

import cz.uhk.goodsservice.model.Good;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodRepository extends JpaRepository<Good, Integer> {
    Good findById(final int id);
}
