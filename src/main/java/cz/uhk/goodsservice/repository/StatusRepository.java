package cz.uhk.goodsservice.repository;

import cz.uhk.goodsservice.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Integer> {
    Status findById(final int id);
}
