package cz.uhk.goodsservice.controller;

import cz.uhk.goodsservice.model.Good;
import cz.uhk.goodsservice.model.Status;
import cz.uhk.goodsservice.repository.GoodRepository;
import cz.uhk.goodsservice.repository.StatusRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class GoodController {
    final GoodRepository goodRepository;
    final StatusRepository statusRepository;

    public GoodController(final GoodRepository goodRepository, final StatusRepository statusRepository) {
        this.goodRepository = goodRepository;
        this.statusRepository = statusRepository;
    }

    @GetMapping("/goods")
    public ResponseEntity<List> getGoods() {
        return new ResponseEntity<>(goodRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/good")
    public ResponseEntity<Good> getGood(final int id) {
        return new ResponseEntity<>(goodRepository.findById(id), HttpStatus.OK);
    }

    @PostMapping("/createGood")
    public ResponseEntity<List> createGood(@Valid final Good good, @RequestParam final int statusID, final BindingResult result) {
        if (result.hasErrors()) {
            final List<String> errors = new ArrayList<>();
            for (ObjectError error : result.getAllErrors()) {
                errors.add(error.getObjectName() + " : " + error.getDefaultMessage());
            }
            return new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
        }
        if (statusID < 1) {
            final List<String> errors = new ArrayList<>();
            errors.add("ID stavu musí být vyplněno.");
            return new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
        }
        final Good newGood = new Good(good.getName(), good.getQuantity(), good.getPrice(), statusRepository.findById(statusID));
        goodRepository.save(newGood);
        final List<String> success = new ArrayList<>();
        return new ResponseEntity<>(success, HttpStatus.OK);
    }


    @PostMapping("/changeGood")
    public ResponseEntity<List> changeGood(@RequestParam final Good good, final BindingResult result) {
        if (result.hasErrors()) {
            final List<String> errors = new ArrayList<>();
            for (ObjectError error : result.getAllErrors()) {
                errors.add(error.getObjectName() + " : " + error.getDefaultMessage());
            }
            return new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
        }
        final Good changedGood = goodRepository.findById(good.getId());
        if (changedGood == null) {
            final List<String> errors = new ArrayList<>();
            errors.add("Chybí ID změněného zboží.");
            return new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
        }
        changedGood.setName(good.getName());
        changedGood.setPrice(good.getPrice());
        changedGood.setQuantity(good.getQuantity());
        changedGood.setPrice(good.getPrice());
        goodRepository.save(good);
        final List<String> success = new ArrayList<>();
        return new ResponseEntity<>(success, HttpStatus.OK);
    }

    @PostMapping("/changeStatusOfGood")
    public ResponseEntity<String> changeStatusOfGood(@RequestParam final int statusId, @RequestParam final int GoodId) {
        if (statusId < 1 || GoodId < 1) {
            return new ResponseEntity<>("ID nesmí být menší než 1.", HttpStatus.NOT_ACCEPTABLE);
        }
        final Good good = goodRepository.findById(GoodId);
        if (good == null) {
            return new ResponseEntity<>("Zadané ID zboží nepatří k žádnému zboží.", HttpStatus.NOT_ACCEPTABLE);
        }
        final Status status = statusRepository.findById(statusId);
        if (status == null) {
            return new ResponseEntity<>("Zadané ID stavu nepatří k žádnému zboží.", HttpStatus.NOT_ACCEPTABLE);
        }
        good.setStatus(status);
        goodRepository.save(good);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping("/createOrUpdateGoodInternal")
    public ResponseEntity<Boolean>createOrUpdateGoodInternal(@RequestParam final int id,@RequestParam final int quantity){
      final Good good= goodRepository.findById(id);
        good.setQuantity(quantity);
        goodRepository.save(good);
        return new ResponseEntity<>(true,HttpStatus.OK);
    }

    @GetMapping("/getGoodById")
    public ResponseEntity<Good>getGoodById(@RequestParam final int id){
      return new ResponseEntity<>( goodRepository.findById(id),HttpStatus.OK);
    }
}
